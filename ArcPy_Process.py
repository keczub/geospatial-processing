import arcpy
import os
import time

def createGDBwithPointLayer():
    """
    Function creates Feature Class with point geometry type and fields necessary to store output of further functions
    As a parameter global variable PATH_TO_TEMP_GDB is taken. 
    No output is returned only Feature Class in defined path.
    """

    arcpy.CreateFeatureclass_management(PATH_TO_TEMP_GDB, "frame_point_layer", "POINT", spatial_reference=SPATIAL_REFERENCE)
    fields_list = [["LAYERNAME", "TEXT"],["FEAT_NUMBER", "SHORT"],["PT_NUMBER", "SHORT"],
                    ["P_USELESS", "SHORT"],["USELESS", "SHORT"], ["PRE_USELESS", "FLOAT"]]
    for field_name, field_type in fields_list:
        arcpy.AddField_management(PATH_TO_TEMP_GDB+"\\frame_point_layer", field_name, field_type, field_is_nullable="NULLABLE")

        
def fillPointLayerWithExistingPoints():
    """
    Function fills frame_point_layer with points and their characteristic (origination layer name, feature number).
    During execution it iterates through existing Feature Classes with geometry type polygon or polyline and extracts vertices.
    Every vertex is stored in table with its identifier. Additionally information with layer_name and geometry is stored in 
    layername_layergeomtype_dict. 
    """

    arcpy.env.workspace = PATH_TO_ANALYZE_GDB + FEATURE_DATASET_NAME
    fc = arcpy.ListFeatureClasses()
    layername_layergeomtype_dict = {}
    
    for number, layer in enumerate(fc):

        desc = arcpy.Describe(PATH_TO_ANALYZE_GDB + FEATURE_DATASET_NAME +str(layer))
        geomtype = str(desc.shapeType)
        layername_layergeomtype_dict[layer] = geomtype
        frame_point_layer_fields = ["SHAPE@", "LAYERNAME", "FEAT_NUMBER", "PT_NUMBER", "P_USELESS", "USELESS", "PRE_USELESS"]
        
        with arcpy.da.Editor(PATH_TO_TEMP_GDB) as edit:
            if geomtype == "Polyline" or geomtype == "Polygon":
                wynik = arcpy.FeatureVerticesToPoints_management(PATH_TO_ANALYZE_GDB + FEATURE_DATASET_NAME +str(layer),
                                                                 "in_memory"+"\\"+"layer_" + str(number), "ALL")
                
                with arcpy.da.InsertCursor(PATH_TO_TEMP_GDB+"\\frame_point_layer", frame_point_layer_fields ) as cursor:
                    last_origin_fid = 1
                    counter = 1
                    for i in arcpy.da.SearchCursor(wynik, ["SHAPE@", "OID@", "ORIG_FID"]):
                            if i[2] != last_origin_fid:
                                counter = 1
                            cursor.insertRow([i[0], str(layer), i[2], counter, 0, 0, 0])
                            counter += 1
                            last_origin_fid = i[2]

            elif geomtype == "Point":
                with arcpy.da.InsertCursor(PATH_TO_TEMP_GDB+"\\frame_point_layer", frame_point_layer_fields ) as cursor:
                    for i in arcpy.da.SearchCursor(PATH_TO_ANALYZE_GDB + FEATURE_DATASET_NAME +str(layer), ["SHAPE@", "OID@"]):
                            cursor.insertRow([i[0], str(layer), i[1], 1, 0, 0, 0])
            else:
                pass
    
    return layername_layergeomtype_dict
                
def findExceededVertices_v3(polyline_geom, polyline_nr, layer_name):
    """
    Function marks potentially useless vertices in frame_point_layer
    input:
        polyline_geom - arcpy.arcobjects.geometries.Polyline
        polyline_nr - int, segment number 
        layer_name - str, layer name 
    """

    try:
        polyline_geom_length = polyline_geom.pointCount
        for i in range(polyline_geom_length-2):
            short_line_array = [point_array for point_array in polyline_geom][0]

            short_line_3 = [short_line_array[i], short_line_array[i+1], short_line_array[i+2]]
            short_line_3_polyline = arcpy.Polyline(arcpy.Array(short_line_3), SPATIAL_REFERENCE)
            short_line_3_polyline_length = short_line_3_polyline.getLength()
            

            short_line_2 = [short_line_array[i], short_line_array[i+2]]
            short_line_2_polyline = arcpy.Polyline(arcpy.Array(short_line_2), SPATIAL_REFERENCE)
            short_line_2_polyline_length = short_line_2_polyline.getLength()
            
            ratio = (short_line_3_polyline_length - short_line_2_polyline_length)/short_line_3_polyline_length
            if  ratio < 0.0005:
                with arcpy.da.Editor(PATH_TO_TEMP_GDB) as edit:
                    SQL = "LAYERNAME = " +"'"+ layer_name+"'" + " AND " + "FEAT_NUMBER = " +str(polyline_nr)+ " AND " + "PT_NUMBER = " + str(i+2)
                    cursor = arcpy.da.UpdateCursor(PATH_TO_TEMP_GDB+"\\frame_point_layer", ["P_USELESS", "PRE_USELESS"], SQL)
                    for row in cursor:
                            row[0] = 1
                            row[1] = ratio
                            cursor.updateRow(row)
                    del(cursor)
    except:
        print("Error for: ", str(polyline_nr), " ", str(layer_name))

def modifyPolygonToLine(x):
    """
    Function converts polygon geometry to polyline by removing last vertex
    input:
        polygone_geom - arcpy.arcobjects.geometries.Polygon
    output:
        polyline_geom - arcpy.arcobjects.geometries.Polyline
    """
    
    polygon_array = [j for j in x][0]
    polygon_vertices_count = polygon_array.count
    polygon_array.remove(polygon_vertices_count-1)
    polyline_geom = arcpy.Polyline(polygon_array, SPATIAL_REFERENCE)
    return polyline_geom     

def selectFeaturesFromLayersToAnalyze():
    """
    Function iterates through existing layers and calls function findExceededVertices_v3 in order to find exceeded vertices
    """
    arcpy.env.workspace = PATH_TO_ANALYZE_GDB + FEATURE_DATASET_NAME
    fc = arcpy.ListFeatureClasses()
 
    for layer in [str(x) for x in fc]:
        for ID_nr, geom in arcpy.da.SearchCursor(layer, ["OID@", "SHAPE@"]):
            if geom.type == u'polyline': # Polyline
                if geom.pointCount == 2:
                    continue
                else:
                    findExceededVertices_v3(geom, ID_nr, layer)
            elif geom.type == u'polygon':  # Polygon
                polyline = modifyPolygonToLine(geom)
                findExceededVertices_v3(polyline, ID_nr, layer)
            elif geom.type == u'point': # Point
                continue
            else:
                print ("Different geometry")

def markOverlayingExceededPointGroups():
    """
    Function matches overlaying points to group them and mark them as useless if all of them meet criteria 
    """
    arcpy.FindIdentical_management(PATH_TO_TEMP_GDB + "\\frame_point_layer", "in_memory" + "\\output_fd","Shape", "0.2 Meters")
    arcpy.JoinField_management(PATH_TO_TEMP_GDB + "\\frame_point_layer", "OID", "in_memory" + "\\output_fd", "IN_FID")
    arcpy.AddField_management(PATH_TO_TEMP_GDB + "\\frame_point_layer", "TOSUM", "SHORT")
    arcpy.CalculateField_management(PATH_TO_TEMP_GDB + "\\frame_point_layer", "TOSUM", 1)

    frame_point_layer_stats = arcpy.Statistics_analysis(PATH_TO_TEMP_GDB + "\\frame_point_layer",  "in_memory" + "\\frame_point_layer_stats", [["TOSUM", "SUM"], ["P_USELESS", "SUM"]], "FEAT_SEQ")
    arcpy.JoinField_management(PATH_TO_TEMP_GDB + "\\frame_point_layer", "FEAT_SEQ", frame_point_layer_stats, "FEAT_SEQ")

    with arcpy.da.UpdateCursor(PATH_TO_TEMP_GDB + "\\frame_point_layer", ["USELESS", "SUM_TOSUM", "SUM_P_USELESS"]) as cursor:
        for row in cursor:
            if row[1] == row[2]:
                row[0] = 1
                cursor.updateRow(row)

def analyzeLayersInGroups(layername_layergeomtype_dict):
    """
    Function analyzes sequences of layers that may or may not have common overlapping vertices
    input:
        layername_layergeomtype_dict - dictionary with key layer name and value geomtype as string e.g
                                         {u'taxi': 'Polygon', u'industrial': 'Polygon', u'school': 'Point'}
    output: 
        sequences_to_skip - list, with FEAT SEQ numbers to filter 
    """
    arcpy.MakeFeatureLayer_management(PATH_TO_TEMP_GDB + "\\frame_point_layer", "frame_point_layer")
    selection_output_useless = arcpy.SelectLayerByAttribute_management("frame_point_layer", "NEW_SELECTION", "USELESS=1")
    layers_in_seq_dict = {}
    sequences_to_skip = []

    with arcpy.da.SearchCursor(selection_output_useless, ["FEAT_SEQ", "LAYERNAME"]) as cursor:
        for row in cursor:
            if row[0] in layers_in_seq_dict:
                layers_in_seq_dict[row[0]].append(layername_layergeomtype_dict[row[1]])
            else:
                layers_in_seq_dict[row[0]]=[layername_layergeomtype_dict[row[1]]]

    # Set with FEAT_SEQ values with more than one layer e.g. FEAT_SEQ = 399 has two points from different layers
    # sets_to_skip selects them by length of unique values in list
    # it looks like set ([0, 390, 10, 265, ...]) where number is FEAT_SEQ identifier
    
    sets_to_skip = set([0 if len(layers_in_seq_dict[item]) != 2 else item for item in layers_in_seq_dict])

    # Space for topology rules
    # Iteration through list with more than one layer point in aggregate
    # Currently there is a rule for intersection of all polylines

    for item in sets_to_skip:
        if item == 0:
            continue
        else:
            if len(set(layers_in_seq_dict[item])) == 1 and set(layers_in_seq_dict[item]) == set(['Polyline']):
                sequences_to_skip.append(item)
            else:
                continue
                
    return sequences_to_skip

def topologyFilter (sequences_to_skip):
    """
    Function for filtering records according to topology rules
    input:
        sequences_to_skip - list of FEAT_SEQ values that already has been marked to filter
    """
    def createSqlQuery(sequences_to_skip):
        """
        Function to generate SQL QUERY 
        """
        if len(sequences_to_skip) == 0: 
            SQL_query = "USELESS = 1"
        elif len(sequences_to_skip) == 1:   
            SQL_query = "USELESS = 1 AND FEAT_SEQ <>" + str(sequences_to_skip[0])
        else:
            text = ""
            for i in sequences_to_skip:
                text += str(i) + ","
            text = text[:-1]
            SQL_query = "USELESS = 1 AND FEAT_SEQ NOT IN (" + text + ")"
        return SQL_query
    
    sqlQuery = createSqlQuery(sequences_to_skip)
    arcpy.MakeFeatureLayer_management(PATH_TO_TEMP_GDB+"\\frame_point_layer", "frame_point_layer_2")
    selection_output_useless_with_topology_filter = arcpy.SelectLayerByAttribute_management("frame_point_layer_2", "NEW_SELECTION", sqlQuery)
    return selection_output_useless_with_topology_filter

def createPointAndLineLayer(selection_output_useless_with_topology_filter):

    def createSqlQuery(list_to_skip):
    
        if len(list_to_skip) == 0:
            pass
        elif len(list_to_skip) == 1:
            SQL_query = "OID <>" + str(list_to_skip[0])
        else:
            text = ""
            for i in list_to_skip:
                text += str(i) + ","
            text = text[:-1]
            SQL_query = "OID NOT IN (" + text + ")"

        return SQL_query
            
    OID_TO_REMOVE = []
    list_OID, list_LAYERNAME, list_FEAT_NUMBER, list_PT_NNUMBER = [],[],[],[]
    with arcpy.da.SearchCursor(selection_output_useless_with_topology_filter, ['OID', 'LAYERNAME', 'FEAT_NUMBER', 'PT_NUMBER']) as cursor:

        for row in cursor:
            list_OID.append(row[0])
            list_LAYERNAME.append(row[1])
            list_FEAT_NUMBER.append(row[2])
            list_PT_NNUMBER.append(row[3])

    for i in range(len(list_OID)-1):
        if (list_LAYERNAME[i] == list_LAYERNAME[i+1]) and (list_FEAT_NUMBER[i] == list_FEAT_NUMBER[i+1]) and (int(list_PT_NNUMBER[i])+1 ==int(list_PT_NNUMBER[i+1])):
            OID_TO_REMOVE.append(list_OID[i])
            OID_TO_REMOVE.append(list_OID[i+1])

    list_to_skip = [i for i in OID_TO_REMOVE]

    sqlQuery = createSqlQuery(list_to_skip)
    selection_output_useless_with_topology_filter_2 = arcpy.SelectLayerByAttribute_management("frame_point_layer_2", "SUBSET_SELECTION", sqlQuery)
    return selection_output_useless_with_topology_filter_2

def createOutput(selection_output_useless_with_topology_filter_2, OUTPUT_PATH, OUTPUT_NAME):

    arcpy.MakeFeatureLayer_management(selection_output_useless_with_topology_filter_2, "output_2")
    arcpy.CopyFeatures_management("output_2", OUTPUT_PATH + OUTPUT_NAME)

if __name__ == "__main__":

    SPATIAL_REFERENCE = arcpy.SpatialReference(4326)
    CURRENT_PATH = "in_memory" + "\\"
    GDB_TO_ANALYZE = "Sample_OSM_2.gdb"
    PATH_TO_ANALYZE_GDB = "C:\Users\keczub\Documents\Remove_exceeding_vertexes_11\Sample_Data_Preparation\Sample_OSM_2.gdb"
    TEMP_GDB_NAME = "Temporary.gdb"
    PATH_TO_TEMP_GDB = "in_memory"
    FEATURE_DATASET_NAME = "\\OSM\\"
    OUTPUT_NAME = "ArcPy_Output_GDB_2"
    OUTPUT_PATH = "C:\\Users\\keczub\\Documents\\Remove_exceeding_vertexes_11\\Output_ArcPy_2\\"
    
    print("CURRENT_PATH:", CURRENT_PATH)
    print("PATH_TO_TEMP_GDB:", PATH_TO_TEMP_GDB)
    
    start = time.time()
    createGDBwithPointLayer()               
    layername_layergeomtype_dict = fillPointLayerWithExistingPoints()
    selectFeaturesFromLayersToAnalyze()
    markOverlayingExceededPointGroups()
    elements_to_skip = analyzeLayersInGroups(layername_layergeomtype_dict)
    selection_output_useless_with_topology_filter = topologyFilter(elements_to_skip)
    selection_output_useless_with_topology_filter_2 = createPointAndLineLayer(selection_output_useless_with_topology_filter)
    createOutput(selection_output_useless_with_topology_filter_2, OUTPUT_PATH, OUTPUT_NAME)
    end = time.time()
    print("Execution time:", int((end-start)/60), "min.")
