### Idea ###

The main idea was to detect unnecessary vertices in existing geometry. Unnecessary vertex is considered as the one that has no influence on geometry shape and is not node with adjecent features.
Its presence may cause topological problems and unexpected errors during geometry display. Problem occurs often when a geometry is updated or a new feature is captured. 
Checking veritces manually is time consuming and tedious. I present a solution below to help automatically detect potential areas of improvement.

### Data ###

Development and testing was conducted on dataset from OpenStreetMap. The data was downloaded from [GEOFABRIK](https://www.geofabrik.de/data/download.html) and clipped to two independent Area of Interest (AOI) located in Gdansk. The first AOI covers area of 70 ha of the Zaspa residental district. The second AOI covers area of 136 ha of the Gdansk central district.The first set contains 1334 features and the second one 2784 ones. Data was downloaded as ESRI Shapefile and loaded to Feature Dataset in ESRI Geodatabase File.

### Implementation ###

The implementation of the idea was performed using two popular programs for spatial data processing: ArcGIS and FME Workbench.
Both of them have the implementation of basic geospatial objects and functions. Additionally user can extend this functionalities with Python scripting language.

Implementation consists of some basic steps:
1. Read the data from ESRI Geodatabase File. 
2. Create a data frame and update it with all existing vertices as points with status 0 - important.  
3. Analyze geometry on a layer and update information in the data frame with vertex status 1 - unnecessary.
4. Group vertices by their location and check if all of them are defined as unnecessary.
5. Filter outcome with the required topology rules (e.g. nodes at polyline intersection are necessary).
6. Filter outcome to select only the points that are without neighbours.

Requirements:
- Input data as ESRI Geodatabase File with Feature Dataset
- FME Wrokbench version 2019 with Python 3.5 to run FME_Process.fmw
- ArcMap version 10.4 with Python 2.7 to run ArcPy_Process.py

![Optional Text](images/FME_Process.png)

### Outcome ###

Both implementations resulted with correct outputs. Slight differences were noticed between the outputs.
The outcome depends on a user and preferred format. Currently it is a point layer.
Detailed summary of the outcome is presented in the table below.

|                                     | Sample_OSM_1.gdb |                    | Sample_OSM_2.gdb |                    |
|-------------------------------------|------------------|--------------------|------------------|--------------------|
|                                     | FME_Output_GDB_1 | ArcPy_Output_GDB_1 | FME_Output_GDB_2 | ArcPy_Output_GDB_2 |
| Number of all vertices in dataset   | 7154             | 7154               | 15169            | 15169              |
| Number of unecessary vertices found | 114              | 111                | 515              | 598                |
| True Positives                      | 111              | 111                | 512              | 592                |
| False Positives                     | 3                | 0                  | 3                | 6                  |

All analyzed vertices are located in the folder ./Analyze_Output where the outputs were enriched with the additional column "Correct". The column contains labels: 1 (correct) and 0 (incorrect). 
Errors for FME Workflow output are located at the border of Area of Interest caused by the clipping operation.
After the analysis a bug with the interesection filter operation in ArcPy script has been found. The bug is planned to be fixed in next commit.  

![Optional Text](images/img_3.png)

![Optional Text](images/img_1_2.png)

### Summary ###

Both implementations are able to perform the invented scenario. During the implementationa I noticed that:

* Creating larger scripts with ArcPy is more difficult than with FME Workbench due to problems with debugging. FME Inspector helps a lot to inspect partial and the last output.
* Processing with FME was slightly faster, because it used more custom transformers. In ArcGIS native functions are more useful in reference to layers.
* ArcGIS has less options (e.g. SQL query selection is limited). In the FME, in contrast to the ArcGIS user can overcome limitations with Python Caller end external interpreter.
* It it difficult to detect all unnecessary vertices. There will be always a trade-off between the performance and coverage. In general it is important to return as many zero false positive examples as possible.
